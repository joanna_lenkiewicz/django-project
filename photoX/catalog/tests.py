from django.test import TestCase

# Create your tests here.
'''
Testing models
'''

from catalog.models import Photo

class PhotoModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        #Set up non-modified objects used by all test methods
        Photo.objects.create(title='Big', description='Photo')

    def test_title_label(self):
        photo=Photo.objects.get(id=1)
        field_label = photo._meta.get_field('title').verbose_name
        self.assertEquals(field_label,'title')

    def test_description_label(self):
        photo=Photo.objects.get(id=1)
        field_label = photo._meta.get_field('description').verbose_name
        self.assertEquals(field_label,'description')

    def test_title_max_length(self):
        photo=Photo.objects.get(id=1)
        max_length = photo._meta.get_field('title').max_length
        self.assertEquals(max_length,200)

    def test_object_name_is_title(self):
        photo=Photo.objects.get(id=1)
        expected_object_name = '%s' % (photo.title)
        self.assertEquals(expected_object_name,str(photo))

    def test_get_absolute_url(self):
        photo=Photo.objects.get(id=1)
        #This will also fail if the urlconf is not defined.
        self.assertEquals(photo.get_absolute_url(),'/catalog/photo/1')


'''
Testing forms
'''

import datetime
from django.utils import timezone
from catalog.forms import MyRegistrationForm

class RegistrationFormTest(TestCase):

    def test_password1_field_label(self):
        form = MyRegistrationForm()
        self.assertTrue(form.fields['password1'].label == None or form.fields['password1'].label == 'Password')
