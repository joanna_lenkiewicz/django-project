from django.conf.urls import url

from . import views
from django.contrib.auth.models import User
from django.views.generic.edit import CreateView
from django.contrib.auth.forms import UserCreationForm


urlpatterns = [
    url(r'^$', views.index, name='index'), #url for index
    url(r'^photos/$', views.PhotoListView.as_view(), name='photos'), #url for photos
    url(r'^photo/(?P<pk>\d+)$', views.PhotoDetailView.as_view(), name='photo-detail'), #url for detail of one photo
    url(r'^users/$', views.UserListView.as_view(), name='users'), #url for users
    url(r'^user/(?P<pk>\d+)$', views.get_key, name='get-key'),
    url(r'^user/(?P<pk>\d+)/update/$', views.ProfileEdit.as_view(), name='edit-profile'),
    url(r'^myphotos/$', views.UserPhotosListView.as_view(), name='my-photos'),
    #url(r'^register/$', register_user, name='register'),
    url(r'^register/$', views.register_user, name='register'),
    url(r'^addphoto/$', views.add_photo, name='add-photo'),
    url(r'^photo/(?P<pk>\d+)/update/$', views.PhotoEdit.as_view(), name='edit-photo'),
    url(r'^photo/(?P<pk>\d+)/delete/$', views.PhotoDelete.as_view(), name='delete-photo'),
    #url(r'^register/$', views.SignUpView.as_view(), name='register'),
    #url('^accounts/', include('django.contrib.auth.urls')),
    #catalog/photo/(?P<pk>\\d+)$

]

"""
    url(r'^register/', CreateView.as_view(
        template_name='auth/register.html',
        form_class=UserCreationForm,
        success_url='/catalog/'
    ), name="register"),

"""
