from django.db import models
from django.contrib.auth.models import User
from datetime import date
from django.contrib.auth.models import AbstractBaseUser
from django.core.urlresolvers import reverse

class Photo(models.Model):
    """
    Model representing a specific photo.
    """
    #id = models.UUIDField(primary_key=True, default=uuid.uuid4, help_text="Unique ID for particular photo")
    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    title = models.CharField(max_length=200)
    description = models.TextField(max_length=1000, help_text="Enter a brief description of the photo")
    date = models.DateField(null=True, blank=True, auto_now_add=True)
    #image = models.TextField(max_length=100000, blank=True, null=True) #blank? null?
    image = models.FileField(null=True, upload_to='photos')

    class Meta:
        ordering = ['-date']

    def get_absolute_url(self):
        """
        Returns the url to access a particular author instance.
        """
        return reverse('photo-detail', args=[str(self.id)])


    def __str__(self):
        """
        String for representing the Model object.
        """
        return self.title
