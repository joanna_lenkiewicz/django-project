from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect
from django.contrib import auth

# Create your views here.
from .models import Photo
from django.contrib.auth.models import User

#view for index.html to which we created url in urls.py
def index(request):
    """
    View function for home page of site.
    """

    # Number of visits to this view, as counted in the session variable.
    num_visits=request.session.get('num_visits', 0)
    request.session['num_visits'] = num_visits+1

    # Render the HTML template index.html with the data in the context variable
    return render(
        request,
        'index.html',
        context={'num_visits':num_visits}, # num_visits appended
    )

from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin

class PhotoListView(LoginRequiredMixin,generic.ListView):
    model = Photo
    paginate_by = 10

class PhotoDetailView(LoginRequiredMixin,generic.DetailView):
    model = Photo

class UserListView(LoginRequiredMixin,generic.ListView):
    model = User
    paginate_by = 10

class UserDetailView(LoginRequiredMixin,generic.ListView):
    model = User
    template_name ='auth/user_detail.html'

def get_key(request, pk):
    uploader=get_object_or_404(User, pk = pk)
    photos = Photo.objects.filter(author=uploader)
    username = uploader.username

    return render(request, 'auth/user_detail.html', {'photos': photos, 'username' : username})

class UserPhotosListView(LoginRequiredMixin,generic.ListView):
    """
    Generic class-based view listing books on loan to current user.
    """
    model = Photo
    template_name ='catalog/user_photo.html'
    paginate_by = 10

    def get_queryset(self):
        return Photo.objects.filter(author=self.request.user)

from .forms import MyRegistrationForm, AddPhotoForm
from django.core.urlresolvers import reverse

def register_user(request):
    # If this is a POST request then process the Form data
    if request.method == 'POST':

        form = MyRegistrationForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            email = form.cleaned_data['email']
            user = User.objects.create_user(username, email, password)
            user.save()

            return HttpResponseRedirect(reverse('photos') )

    # If this is a GET (or any other method) create the default form.
    else:
        form = MyRegistrationForm()

    return render(request, 'auth/register.html', {'form': form})

from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

class PhotoEdit(LoginRequiredMixin, UpdateView):
    model = Photo
    fields = ['title', 'description', 'image']
    template_name= 'catalog/photo_form.html'

class ProfileEdit(LoginRequiredMixin, UpdateView):
    model = User
    fields = ['username','first_name', 'last_name', 'email']
    template_name= 'auth/user_form.html'
    success_url = reverse_lazy('my-photos')

class PhotoDelete(LoginRequiredMixin, DeleteView):
    model = Photo
    success_url = reverse_lazy('my-photos')

def add_photo(request):
    # If this is a POST request then process the Form data
    if request.method == 'POST':
        # Create a form instance and populate it with data from the request (binding):
        form = AddPhotoForm(request.POST, request.FILES)

        # Check if the form is valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required (here we just write it to the model due_back field)
            # Create user and save to the database
            imgfile = request.FILES['image']
            photo = Photo.objects.create()
            photo.author = request.user
            photo.title = form.cleaned_data['title']
            photo.description = form.cleaned_data['description']
            photo.image = imgfile
            photo.save()

            # redirect to a new URL:
            return HttpResponseRedirect(reverse('photos') )

    # If this is a GET (or any other method) create the default form.
    else:
        form = AddPhotoForm()

    return render(request, 'auth/add_photo.html', {'form': form})
"""

from django.core.urlresolvers import reverse
import datetime
from .forms import SignUpForm
from django.core.urlresolvers import reverse_lazy

from braces import views

class SignUpView(views.AnonymousRequiredMixin, views.FormValidMessageMixin, generic.CreateView):
    model = User
    form_class = SignUpForm
    form_valid_message = 'Successfully created your account, ' \
                         'go ahead and login.'
    success_url = reverse_lazy('index')
    template_name = 'auth/register.html'


from .forms import MyRegistrationForm

def register_user(request):
    # If this is a POST request then process the Form data
    if request.method == 'POST':

        # Create a form instance and populate it with data from the request (binding):
        form = MyRegistrationForm(request.POST)

        # Check if the form is valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required (here we just write it to the model due_back field)

            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            email = form.cleaned_data['email']
            # Create user and save to the database
            user = User.objects.create_user(username, email, password)

            # Update fields and then save again
            #user.first_name = form.cleaned_data['first_name']
            #user.last_name = form.cleaned_data['last_name']
            user.save()

            # redirect to a new URL:
            return HttpResponseRedirect(reverse('index') )

    # If this is a GET (or any other method) create the default form.
    else:
        form = MyRegistrationForm()

    return render(request, 'auth/register.html', {'form': form})
"""
